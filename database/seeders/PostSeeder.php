<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($i = 1 ; $i <=100 ; $i++){
            \DB::table("posts")->insert([
                "title" => "Post title {$i}",
                "content" => "Post content {$i}",
                "category_id" => random_int(1,10),
                "user_id" => random_int(1,10)
            ]);
        }
    }
}
