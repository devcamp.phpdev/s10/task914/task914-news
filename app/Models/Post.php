<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Casts\Attribute;

class Post extends Model
{
    use HasFactory;
    public function comments(): HasMany
    {
        return $this->hasMany(PostComment::class);
    }
    protected function fullHtml(): Attribute
    {
        return Attribute::make(
            get: fn() => $this->title . '<br/>' . $this->content,
        );
    }

    protected function content(): Attribute
    {
        return Attribute::make(
            set: fn(string $value) => strtolower($value)
        );
    }
}
