<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        //$posts = Post::all();
        //$posts = \DB::table('posts')->where("id", "<=", "4")->get();
        //$posts = \DB::table('posts')->whereBetween("id",[3,7])->get();
        //$posts = \DB::table('posts')->whereIn("id", [2,4,6])->get();
        $posts = \DB::table('posts')->whereYear("created_at", "=", 2024)->get();
        return response()->json($posts);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'title' => 'required|max:255',
            'content' => 'required',
        ]);
        //Post::create($request->all());
        \DB::table('posts')->insert($request->toArray());

        return response()->json(["message" => "success"]);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        /*
        $post = Post::find($id);
        return response()->json($post);
        */
        /*
        $post = Post::find($id);
        $comments = $post->comments;
        */
        ///$comments = Post::with('comments')->find($id);
        /*
        $comments = Post::find($id)->comments;
        return response()->json($comments);
        */
        /*
        $post = Post::find($id);
        die ($post->full_html);
        */
        $post = Post::find($id);
        $post->content = "New Post Content";
        die($post->content);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'title' => 'required|max:255',
            'content' => 'required',
        ]);
        /*
        $post = Post::find($id);
        $post->update($request->all());
        */
        \DB::table('posts')->where("id", "=", $id)->update($request->toArray());

        return response()->json(["message" => "update success"]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
        /*
        $post = Post::find($id);
        $post->delete();
        */
        \DB::table('posts')->where("id", "=", $id)->delete();
        return response()->json(["message" => "delete success"]);
    }
}